---
title: "Velkommen!"
date: 2021-12-13
type: ["posts"]
draft: false
tags:
categories:
---

Velkommen til ROAM!

Her vil du finne info om fjelløping, terrengløpesko, konkurranser med mer. 

[Ta kontakt](mailto:roam_running@protonmail.com) om du har spørsmål eller forslag!
